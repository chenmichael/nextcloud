# Adapted from https://gitlab.com/chenmichael/reportgenerator/-/blob/main/.gitlab-ci.yml

image: mcr.microsoft.com/dotnet/sdk:7.0

default:
  tags:
    - docker

# Prevent duplicate pipelines (see https://gitlab.com/gitlab-org/gitlab/-/issues/300146)
workflow:
  rules:
    # forbid merge request pipelines
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - when: always

variables:
  DOCKERFILE_PATH:
    description: Specify the path of the Dockerfile.
    value: Dockerfile
    expand: true
  DOCKER_CONTEXT:
    description: Specify the path of the docker build context.
    value: "."
    expand: true
  DOCKER_TLS_CERTDIR:
    description: Specify to Docker where to create the certificates. (Requires volume mount in runner config)
    value: /certs
  TARGET_PLATFORMS:
    description: Specify the Docker image target platforms.
    value: linux/386,linux/amd64,linux/arm/v5,linux/arm/v7,linux/arm64/v8,linux/mips64le,linux/ppc64le,linux/s390x
  BUILDER_NAME:
    description: Specify the name of the docker builder instance.
    value: nextcloudbuilder
  CONTEXT_NAME:
    description: Specify the name of the docker build context.
    value: nextcloudcontext

tag-analysis:
  image: registry.gitlab.com/chenmichael/analyzesemver:1
  stage: build
  rules:
    - &is-tag
      if: $CI_COMMIT_TAG
  script:
    - analyze --no-prerelease "${CI_COMMIT_TAG}" | tee tag.env
  artifacts:
    reports:
      dotenv:
        - tag.env
    expire_in: never

.docker-job:
  stage: build
  # Use the official docker image.
  image: docker:24-git
  tags:
    - docker
    - privileged
  rules:
    - if: $CI_COMMIT_REF_PROTECTED
  services:
    - docker:24-dind
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - docker context inspect "${CONTEXT_NAME}" || docker context create "${CONTEXT_NAME}"
    - docker buildx inspect "${BUILDER_NAME}" || docker buildx create --name "${BUILDER_NAME}" --platform "${TARGET_PLATFORMS}" --use "${CONTEXT_NAME}"

docker-tag-build:
  extends:
    - .docker-job
  rules: &protected-tags
    # Run on protected tags
    - if: $CI_COMMIT_REF_PROTECTED && $CI_COMMIT_TAG
  needs:
    - job: tag-analysis
  script:
    - docker buildx build
      --pull
      --platform "${TARGET_PLATFORMS}"
      --tag "${CI_REGISTRY_IMAGE}"
      --tag "${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR}"
      --tag "${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR_MINOR}"
      --tag "${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR_MINOR_PATCH}"
      --build-arg "NEXTCLOUD_VERSION=${RELEASE_MAJOR_MINOR_PATCH}"
      --file "${DOCKERFILE_PATH}"
      --provenance false
      --push "${DOCKER_CONTEXT}"

release_job:
  stage: deploy
  image: chenio/release-cli:latest
  rules: *protected-tags
  script:
    - echo "Running release_job"
  needs:
    - job: tag-analysis
    - job: docker-tag-build
  release:
    name: Version ${RELEASE_MAJOR_MINOR_PATCH}
    description: >-
      Nextcloud version ${RELEASE_MAJOR_MINOR_PATCH}.

      This image is tagged with a fixed tag
      name ${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR_MINOR_PATCH} as well as
      floating minor ${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR_MINOR}
      and major ${CI_REGISTRY_IMAGE}:${RELEASE_MAJOR} versions.
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_SHA
    assets:
      links:
        - name: Docker images
          url: ${CI_PROJECT_URL}/container_registry
          link_type: image
